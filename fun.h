#ifndef fun
#define fun


#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_sf_expint.h>
#include <gsl/gsl_sort_vector.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <utility>

using namespace std;
#define N 1
#define CE 0.5772156649

void set_matrix(gsl_matrix* A, double epsilon, double t, double U, double K, double V, double J);

// a->alfa
double S(double R, double a);
double Sbar(double R, double a);

double Ei(double x);

double eprim(double R, double a);
double tprim(double R, double a);
double Uprim(double R, double a);
double Kprim(double R, double a);
double Vprim(double R, double a);
double Jprim(double R, double a);

// b-> beta, g-> gamma
double b(double R, double a);
double g(double R, double a);

double e(double R, double a);
double t(double R, double a);
double U(double R, double a);
double K(double R, double a);
double V(double R, double a);
double J(double R, double a);

gsl_vector* diagonalize(gsl_matrix * A, double epsilon, double t, double U, double K, double V, double J);

pair <double, gsl_vector> optimalize(double R);

void to_file(vector <double> R, vector <double> E_, vector <double> alfa, vector <gsl_vector> E, vector < vector<double> > par);


#endif
