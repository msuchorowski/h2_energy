#include "fun.h"

gsl_vector* diagonalize(gsl_matrix * A, double epsilon, double t, double U, double K, double V, double J){
    
    set_matrix(A, epsilon, t, U, K, V, J);
 
    gsl_eigen_symm_workspace* workspace;
    workspace = gsl_eigen_symm_alloc(6);
    gsl_vector* eigenvalues = gsl_vector_alloc(6);
    gsl_eigen_symm(A, eigenvalues, workspace);
    gsl_eigen_symm_free(workspace);
    return eigenvalues;

    

    }

