# Makefile # (C) Maciej Wołoszyn 2017

##############################################################
### USTAWIENIA :

ARCHIVE := Suchorowski_Michal.tgz

##############################################################


PROG := eigen

CXXFLAGS := -Wall -L/usr/local/lib -lgsl -lgslcblas -lm -I/usr/local/include 
CXX := g++
RM := rm -f
SRC = $(wildcard *.cpp)
OBJECTS = $(SRC:%.cpp=%.o)
CXXLINKLIBS := -L/usr/local/lib -lgsl -lgslcblas -lm -I/usr/local/include 
all : $(PROG)

$(PROG) : $(OBJECTS)
	$(CXX) $(OBJECTS) $(CXXLINKLIBS) -o $(PROG)

# include automatically generated dependencies:
-include $(SRC:%.cpp=%.d)


run :
	./$(PROG)

test :
	valgrind ./$(PROG)

tgz :
	tar cvzf $(ARCHIVE) *.cpp *.h

clean:
	$(RM) *.o

cleanall:
	$(RM) *~ *.o *.d $(PROG)

# end
