#include "fun.h"



void set_my_matrix(gsl_matrix* A, double epsilon, double t, double U, double K, double V, double J){
 
    gsl_matrix_set(A,0,0, 2*epsilon +U );
    gsl_matrix_set(A,1,0, t +V );
    gsl_matrix_set(A,2,0, t +V );
    gsl_matrix_set(A,3,0, J );
    gsl_matrix_set(A,4,0, 0);
    gsl_matrix_set(A,5,0, 0);
 
    gsl_matrix_set(A,0,1, t +V );
    gsl_matrix_set(A,1,1, 2*epsilon +K );
    gsl_matrix_set(A,2,1, -J );
    gsl_matrix_set(A,3,1, V +t );
    gsl_matrix_set(A,4,1, 0);
    gsl_matrix_set(A,5,1, 0);
 
    gsl_matrix_set(A,0,2, t +V );
    gsl_matrix_set(A,1,2, -J );
    gsl_matrix_set(A,2,2, 2*epsilon +K );
    gsl_matrix_set(A,3,2, V +t );
    gsl_matrix_set(A,4,2, 0);
    gsl_matrix_set(A,5,2, 0);
 
    gsl_matrix_set(A,0,3, J );
    gsl_matrix_set(A,1,3, t +V );
    gsl_matrix_set(A,2,3, t +V );
    gsl_matrix_set(A,3,3, 2*epsilon +U );
    gsl_matrix_set(A,4,3, 0);
    gsl_matrix_set(A,5,3, 0);
 
    gsl_matrix_set(A,0,4, 0);
    gsl_matrix_set(A,1,4, 0);
    gsl_matrix_set(A,2,4, 0);
    gsl_matrix_set(A,3,4, 0);
    gsl_matrix_set(A,4,4, 2*epsilon +K -J );
    gsl_matrix_set(A,5,4, 0);
 
    gsl_matrix_set(A,0,5, 0);
    gsl_matrix_set(A,1,5, 0);
    gsl_matrix_set(A,2,5, 0);
    gsl_matrix_set(A,3,5, 0);
    gsl_matrix_set(A,4,5, 0);
    gsl_matrix_set(A,5,5, 2*epsilon +K -J );
 

}


void set_matrix(gsl_matrix* A, double epsilon, double t, double U, double K, double V, double J){
 
    gsl_matrix_set(A,0,0, 2*epsilon +K+J );
    gsl_matrix_set(A,1,0, 2*(t +V) );
    gsl_matrix_set(A,2,0, 0 );
    gsl_matrix_set(A,3,0, 0 );
    gsl_matrix_set(A,4,0, 0);
    gsl_matrix_set(A,5,0, 0);
 
    gsl_matrix_set(A,0,1, 2*(t +V) );
    gsl_matrix_set(A,1,1, 2*epsilon +U+J );
    gsl_matrix_set(A,2,1, 0 );
    gsl_matrix_set(A,3,1, 0 );
    gsl_matrix_set(A,4,1, 0);
    gsl_matrix_set(A,5,1, 0);
 
    gsl_matrix_set(A,0,2, 0 );
    gsl_matrix_set(A,1,2, 0 );
    gsl_matrix_set(A,2,2, 2*epsilon +U -J );
    gsl_matrix_set(A,3,2, 0);
    gsl_matrix_set(A,4,2, 0);
    gsl_matrix_set(A,5,2, 0);
 
    gsl_matrix_set(A,0,3, 0 );
    gsl_matrix_set(A,1,3, 0 );
    gsl_matrix_set(A,2,3, 0 );
    gsl_matrix_set(A,3,3, 2*epsilon +K -J );
    gsl_matrix_set(A,4,3, 0);
    gsl_matrix_set(A,5,3, 0);
 
    gsl_matrix_set(A,0,4, 0);
    gsl_matrix_set(A,1,4, 0);
    gsl_matrix_set(A,2,4, 0);
    gsl_matrix_set(A,3,4, 0);
    gsl_matrix_set(A,4,4, 2*epsilon +K -J );
    gsl_matrix_set(A,5,4, 0);
 
    gsl_matrix_set(A,0,5, 0);
    gsl_matrix_set(A,1,5, 0);
    gsl_matrix_set(A,2,5, 0);
    gsl_matrix_set(A,3,5, 0);
    gsl_matrix_set(A,4,5, 0);
    gsl_matrix_set(A,5,5, 2*epsilon +K -J );
 

}