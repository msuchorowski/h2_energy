#include "fun.h"


double S(double R, double a){
    return exp(-a*R)*(1+a*R+1.0/3.0*pow(a,2)*pow(R,2));
}


double Sbar(double R, double a){
    return exp(a*R)*(1-a*R+1.0/3.0*pow(a,2)*pow(R,2));
}

double Ei(double x){
    return gsl_sf_expint_Ei(x);
    //return 1;
}


double eprim(double R, double a){
    return pow(a,2)-2*a-2.0/R+2*(a+1.0/R)*exp(-2*a*R);
}
double tprim(double R, double a){
    return pow(a,2)*exp(-a*R)*(1+a*R-1.0/3.0*pow(a,2)*pow(R,2))-4*a*exp(-a*R)*(1+a*R);
}
double Uprim(double R, double a){
   // return 0;
    return 5.0/4.0*a;
}
double Kprim(double R, double a){
    //return 0;
   // return 2.0/R-a*exp(-2*a*R)*(2.0/(a*R)+3.0/2.0*a*R+1.0/3.0*pow(a,2)*pow(R,2)+11.0/4.0);
 //  cout << (1-exp(-2*a*R)) << endl;
    return 2.0/R*(1-exp(-2*a*R))-a*exp(-2*a*R)*(3.0/2.0*a*R+1.0/3.0*pow(a,2)*pow(R,2)+11.0/4.0);
}
double Vprim(double R, double a){
  //  return 0;
    return a*exp(-a*R)*(2*a*R+0.25+5.0/(8.0*a*R))-1.0/4.0*a*exp(-3*a*R)*(1+5.0/(2.0*a*R));
 //   return a*exp(-a*R)*(2*a*R+0.25)-1.0/4.0*a*exp(-3*a*R)*(1)+a*5.0/(8.0*a*R)*(exp(-a*R)-exp(-3*a*R));

}
double Jprim(double R, double a){
   // return 0;
    return (a*exp(-2*a*R)*(5.0/4.0-23.0/10.0*a*R-6.0/5.0*pow(a,2)*pow(R,2)-2.0/15.0*pow(a,3)*pow(R,3))+12.0/(5.0*R)*(pow(S(R,a),2)*CE+pow(S(R,a),2)*log(a*R)-2*S(R,a)*Sbar(R,a)*Ei(-2*a*R)+pow(Sbar(R,a),2)*Ei(-4*a*R)));
}


double b(double R, double a){
    return 1.0/sqrt(2)*sqrt((1+sqrt(1-pow(S(R,a),2)))/(1-pow(S(R,a),2)));
    //return 1.0/sqrt(2)*sqrt((1+sqrt(1-Sbar(R,a)*S(R,a))/(1-Sbar(R,a)*S(R,a))));
}
double g(double R, double a){
    return S(R,a)/(1+sqrt(1-pow(S(R,a),2)));
    //return S(R,a)/(1+sqrt(1-Sbar(R,a)*S(R,a)));

}


double e(double R, double a){
    return pow(b(R,a),2)*(1+pow(g(R,a),2))*eprim(R,a)-2*pow(b(R,a),2)*g(R,a)*tprim(R,a);
}
double t(double R, double a){
    return pow(b(R,a),2)*(1+pow(g(R,a),2))*tprim(R,a)-2*pow(b(R,a),2)*g(R,a)*eprim(R,a);
}
double U(double R, double a){
    //return 0;
   // return pow(b(R,a),4)*((1+pow(g(R,a),4))*Uprim(R,a)+2*pow(g(R,a),2)*Kprim(R,a)-4*g(R,a)*(1+pow(g(R,a),2))*Vprim(R,a)+4*pow(g(R,a),2)*Jprim(R,a));
  return pow(b(R,a),4)*((1+pow(g(R,a),4))*Uprim(R,a)+2*pow(g(R,a),2)*Kprim(R,a)-4*g(R,a)*(1+pow(g(R,a),2))*Vprim(R,a)+4*pow(g(R,a),2)*Jprim(R,a));
  
}
double K(double R, double a){
   // return 0;
   // return pow(b(R,a),4)*((1+pow(g(R,a),4))*Kprim(R,a)+2*pow(g(R,a),2)*Uprim(R,a)-4*g(R,a)*(1+pow(g(R,a),2))*Vprim(R,a)+4*pow(g(R,a),2)*Jprim(R,a));
  return pow(b(R,a),4)*((1+pow(g(R,a),4))*Kprim(R,a)+2*pow(g(R,a),2)*Uprim(R,a)-4*g(R,a)*(1+pow(g(R,a),2))*Vprim(R,a)+4*pow(g(R,a),2)*Jprim(R,a));
  
}
double J(double R, double a){
   // return 0;
    return pow(b(R,a),4)*(2*pow(g(R,a),2)*Uprim(R,a)+2*pow(g(R,a),2)*Kprim(R,a)-4*g(R,a)*(1+pow(g(R,a),2))*Vprim(R,a)+pow((1+pow(g(R,a),2)),2)*Jprim(R,a));
   // return pow(b(R,a),4)*(2*pow(g(R,a),2)*Uprim(R,a)+2*pow(g(R,a),2)*Kprim(R,a)-4*g(R,a)*(1+pow(g(R,a),2))*0+pow((1+pow(g(R,a),2)),2)*Jprim(R,a));

}
double V(double R, double a){
   // return 0;
    return pow(b(R,a),4)*(-g(R,a)*(1+pow(g(R,a),2))*Uprim(R,a)-g(R,a)*(1+pow(g(R,a),2))*Kprim(R,a)+(1+6*pow(g(R,a),2)+pow(g(R,a),4))*Vprim(R,a)-2*g(R,a)*(1+pow(g(R,a),2))*Jprim(R,a));
//  return pow(b(R,a),4)*(-g(R,a)*(1+pow(g(R,a),2))*Uprim(R,a)-g(R,a)*(1+pow(g(R,a),2))*Kprim(R,a)+(1+6*pow(g(R,a),2)+pow(g(R,a),4))*0-2*g(R,a)*(1+pow(g(R,a),2))*Jprim(R,a));

}

