#include "fun.h"


int main(){

    //double a=1;
    //double R=0.00001;
    //double test=(a*exp(-2*a*R)*(5.0/4.0-23.0/10.0*a*R-6.0/5.0*pow(a,2)*pow(R,2)-2.0/15.0*pow(a,3)*pow(R,3))+12.0/(5.0*R)*(pow(S(R,a),2)*CE+pow(S(R,a),2)*log(a*R)-2*S(R,a)*Sbar(R,a)*Ei(-2*a*R)+pow(Sbar(R,a),2)*Ei(-4*a*R)));
   // cout << test << endl;
    
    vector <double> vecalpha;
    vector <double> vecR;
    vector <double> vecEG;
    vector <gsl_vector> vecE;
    //parameters in order e,t,U,K,J,V
    vector < vector<double> > vecpar;
    vector <double> T(14);
    pair <double, gsl_vector> p;
    
    double Rmin=0.00001;
    double Rmax=8.0;
    int n1=100;
    int n2=200;
    double dR=(1.0-Rmin)/n1;

    for(double i=Rmin; i<Rmax; i=i+dR){
        if(i>=1.0) dR=(Rmax-1.0)/n2;
        p=optimalize(i);
        vecR.push_back(i);
        vecalpha.push_back(p.first);
        gsl_sort_vector(&p.second);
        vecE.push_back((p.second));
        vecEG.push_back(gsl_vector_min(&p.second));
        T[0]=e(i,vecalpha.back());
        T[1]=t(i,vecalpha.back());
        T[2]=U(i,vecalpha.back());
        T[3]=K(i,vecalpha.back());
        T[4]=J(i,vecalpha.back());
        T[5]=V(i,vecalpha.back());
        T[6]=eprim(i,vecalpha.back());
        T[7]=tprim(i,vecalpha.back());
        T[8]=Uprim(i,vecalpha.back());
        T[9]=Kprim(i,vecalpha.back());
        T[10]=Jprim(i,vecalpha.back());
        T[11]=Vprim(i,vecalpha.back());
        T[12]=b(i,vecalpha.back());
        T[13]=g(i,vecalpha.back());
        vecpar.push_back(T);

    }

    to_file(vecR, vecEG, vecalpha, vecE, vecpar);
    
  cout << "plotting" << endl;
  system("gnuplot gnplot/plotER.p");
  system("gnuplot gnplot/plotEminR.p");
  system("gnuplot gnplot/plotalfaR.p");
  system("gnuplot gnplot/plotPar.p");
  system("gnuplot gnplot/plotVJR.p");
  system("gnuplot gnplot/plotParprim.p");
  system("gnuplot gnplot/plotbetagamma.p");
  //system("gnuplot gnplot/plotEialfa.p");
  //system("gnuplot gnplot/plotEi.p");
  system("gnuplot gnplot/plotR.p");
  //system("gnuplot gnplot/Ei_test.p");

  cout << "plotted" << endl;


  return 0;

}



