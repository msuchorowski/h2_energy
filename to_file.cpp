#include "fun.h"


void to_file(vector <double> R, vector <double> E_, vector <double> alfa, vector <gsl_vector> E, vector < vector<double> > par){
     cout << "saving to file" << endl;
     ofstream file("data.dat");
     for(int i=0; i<alfa.size(); i++){
         file << R[i] << " " << E_[i]+2/R[i] << " " << alfa[i] << "   ";
         for(int j=0; j<6; j++)
                file << gsl_vector_get(&E[i],j)+2/R[i] << " ";
         file << "   "; 
         for(int j=0; j<14; j++)
                file << par[i][j] << " ";
         //file << endl;
         file << Ei(-2*alfa[i]*R[i]) << " " << Ei(-2*R[i]) <<  " " << i << endl;           
     }


    cout << "saved to file" << endl;
    file.close();
   
    

    ofstream file2("Ei.dat");
    for(double j=-4; j<4; j=j+0.01){
       if(j==0) j=j+0.01;
       file2 << j << " " << Ei(j) << endl;}
    file2.close();



}


