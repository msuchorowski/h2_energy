#include "fun.h"



pair <double, gsl_vector> optimalize(double R){
    double amin=1.0;
    double amax=3.0;
    double n=2000;
    double da=(amax-amin)/n;
    double minE=1;
    double minalpha;
    gsl_matrix * A = gsl_matrix_calloc(6,6);
    gsl_vector* En = gsl_vector_alloc(6);

    for(double i=amin; i<amax; i=i+da){
        En=diagonalize(A, e(R,i), t(R,i), U(R,i), K(R,i), V(R,i), J(R,i));
        
        if(i==amin || gsl_vector_min(En)<minE) {
            minE=gsl_vector_min(En);
            minalpha=i;}
    }
    //minalpha=1;
    //cout << "alpha=" << minalpha <<  " with energy=" << minE+2.0/R << " for R=" << R << " beta=" << b(R,minalpha) << endl;
    En=diagonalize(A, e(R,minalpha), t(R,minalpha), U(R,minalpha), K(R,minalpha), V(R,minalpha), J(R,minalpha));
    return make_pair(minalpha, *En);
}
